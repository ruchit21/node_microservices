const course = require("../api/course");
const { CourseRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError } = require("../utils/app-errors");

// All Business logic will be here
class CourseService {
  constructor() {
    this.repository = new CourseRepository();
  }

  async createCourse(UserInputs) {
    try {
      const { title, description, createdBy, purchase_type, price, role } = UserInputs;

      const data = await this.repository.CourseCreate({
        title, description, createdBy, purchase_type, price, role
      });
      
      return FormateData(data);
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async GetCourses() {
    try {
      const data = await this.repository.AllCourses();
      if (data) {
        return FormateData(data);
      } else {
        return {'status': false, 'message': "There was a problem finding the course."}
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async GetSelectedCourse(id) {
    try {
      const data = await this.repository.GetCourse(id);
      return FormateData(data);
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async RateCourse(UserInputs) {
    try {
      const { course_id, id, ratings, description } = UserInputs;

      const data = await this.repository.RateCourse({ course_id, id, ratings, description });
      return FormateData(data);
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async GetPurchasePayload(userId, {courseId}, event) {
    console.log("event", event);
    console.log("courseId", courseId);
    console.log("userId", userId);
    try {
      const course = await this.repository.GetCourse(courseId);
      console.log("course", course);
      
      if (course) {
        const courseData = course.data;
        const payload = {
          event: event,
          data: { userId, courseData }
        }

        return FormateData(payload);
      }
    } catch (error) {
      console.log("error", error);
      throw new APIError(error.message)
    }
  }
}

module.exports = CourseService;
