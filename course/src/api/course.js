const CourseService = require("../services/course-service");
const UserAuth = require("./middlewares/auth");
const { PublishPurchaseEvent } = require("../utils");

module.exports = (app) => {
  const service = new CourseService();

  app.post("/create", UserAuth, async (req, res, next) => {
    try {
      const { id, role } = req.user;
      const { title, description, purchase_type, price } = req.body;

      const course = await service.createCourse({
        title,
        description,
        createdBy: id,
        purchase_type,
        price,
        role: role
      });
      
      return res.json(course);
    } catch (error) {
      next(error);      
    }
  });

  app.get("/", async (req,res, next) => {
    try {
      const { data } = await service.GetCourses();
      return res.json(data);
    } catch (error) {
      next(error.message)
    }
  })

  app.get("/course/:id", async (req, res, next) => {
    try {
      const id = req.params.id;
      const { data } = await service.GetSelectedCourse(id);
      return res.json(data);
    } catch (error) {
      next(error.message)
    }
  });

  app.get("/courses", async (req, res, next) => {
    try {
      const { data } = await service.GetCourses();
      return res.json(data);
    } catch (error) {
      next(error.message)
    }
  });

  app.post("/course-rating/:id", UserAuth, async (req, res, next) => {
    try {
      const course_id = req.params.id;
      const { id } = req.user;
      const { ratings, description } = req.body;

      const data = await service.RateCourse({ course_id, id, ratings, description });
      return res.json(data);
    } catch (error) {
      next(error.message)
    }
  });

  app.post("/cart", UserAuth, async (req, res, next) => {
    try {
      const { id } = req.user;

      const { data } = await service.GetPurchasePayload(
        id,
        { courseId : req.body.id },
        "Add_TO_CART"
      )
      PublishPurchaseEvent(data);

      return res.json(data);
    } catch (error) {
      next(error)
    }
  });

  app.delete("/cart/:id", UserAuth, async (req, res, next) => {
    try {
      const { id } = req.user;
      const courseId = req.params.id;
      console.log("courseId", courseId);

      const { data } = await service.GetPurchasePayload(
        id,
        { courseId},
        "REMOVE_FROM_CART"
      )

      PublishPurchaseEvent(data);

      return res.json(data);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  });
  
};
