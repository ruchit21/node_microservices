// database related modules
module.exports = {
    databaseConnection: require('./connection'),
    CourseRepository: require('./repository/course-repository'),
}