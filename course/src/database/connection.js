const config = require("../config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(config.db, config.user, config.password, {
    host: config.host,
    dialect: config.dialect,
    // operatorsAliases: try,
});