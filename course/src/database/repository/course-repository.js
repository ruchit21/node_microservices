// const { CustomerModel, AddressModel } = require('../models');

const { APIError, BadRequestError, STATUS_CODES } = require('../../utils/app-errors');
const db = require("../models");
const CourseModel = db.course_model;
const CourseRatingModel = db.course_rating_model;

//Dealing with data base operations
class CourseRepository {

    async CourseCreate({ title, description, createdBy, purchase_type, price, role }) {
        try {
            const data = {
                title, description, createdBy, purchase_type, price
            }

            const UserRole = role;

            if (UserRole == "teacher") {
                const course = await CourseModel.create(data);
                if (course) {
                    return { 'status': true, 'message': "Course Created Successfully.", 'data': course};
                } else {
                    return { 'status': false, 'message': "Something went wrong during the user creation process."};
                }
            } else {
                return { 'status': false, 'message': "You Are Not Allowed To Create Course." };
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }

    async AllCourses() {
        try {
            const courses = await CourseModel.findAll();
            if (courses.length > 0) {
                return { 'status': true, 'message': "Successfully located courses.", 'data': courses }
            } else {
                return { 'status': false, 'message': "No courses were found."}
            }
        } catch (error) {
            throw new APIError(error.message)
            
        }
    }

    async GetCourse(id) {
        try {
            const course = await CourseModel.findOne({ where: { id: id } });
            if (course) {
                return { 'status': true, 'message': "Successfully discovered courses", 'data': course }
            } else {
                return { 'status': false, 'message': "No courses were found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async RateCourse({ course_id, id, ratings, description }) {
        try {
            const data = { course_id, rate_by: id, ratings, description };
            const CourseRating = await CourseRatingModel.create(data);

            if (CourseRating) {
                return { 'status': true, 'message': "The course rating was successfully added.", 'data': CourseRating }
            } else {
                return { 'status': false, 'message': "The course rating was not successfully added." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }
}

module.exports = CourseRepository;