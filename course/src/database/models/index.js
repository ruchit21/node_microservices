const config = require("../../config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.db,
    config.user,
    config.password,
    {
        host: config.host,
        dialect: config.dialect,
        operatorsAliases: false,
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.course_model             = require("./course")(sequelize, Sequelize);
db.course_rating_model      = require("./course-ratings")(sequelize, Sequelize);

module.exports = db;