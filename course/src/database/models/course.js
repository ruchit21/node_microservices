module.exports = (sequelize, Sequelize) => {
  const Course = sequelize.define("courses", {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    title: {
      allowNull: false,
      type: Sequelize.STRING
    },
    description: {
      allowNull: false,
      type: Sequelize.STRING
    },
    createdBy: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    purchase_type: {
      type: Sequelize.ENUM,
      values: ["free", "paid"],
      defaultValue: "male"
    },
    price: {
      allowNull: false,
      type: Sequelize.DECIMAL(10,2)
    },
  },
  {
    timestamps: true,
    freezeTableName: true, // Model tableName will be the same as the model name
  });
  return Course;
};
