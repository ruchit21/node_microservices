module.exports = (sequelize, Sequelize) => {
    const Course_ratings = sequelize.define("course_ratings", {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      course_id: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      rate_by: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      ratings: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      description: {
        allowNull: false,
        type: Sequelize.STRING
      },
    },
    {
      timestamps: true,
      freezeTableName: true, // Model tableName will be the same as the model name
    });
    return Course_ratings;
  };
  