const PurchaseService = require("../services/purchase-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new PurchaseService();

  app.get("/cart-list", UserAuth, async (req, res, next) => {
    try {
      const { data } = await service.GetCartdetails();
      return res.json(data);
    } catch (error) {
      next(error);
    }
  });

  app.post("/buy-course", UserAuth, async (req, res, next) => {
    try {
      const { id } = req.user;

      const { data } = await service.PlaceOrder(id);

      return res.json(data);
    } catch (error) {
      next(error);
    }
  })
};
