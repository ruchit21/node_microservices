const PurchaseService = require("../services/purchase-service");

module.exports = (app) => {
  const service = new PurchaseService();

  app.use("/app-events", async (req, res, next) => {
    const { payload } = req.body;

    service.Events(payload);

    console.log("===============  Purchase Service Received Event ====== ");
    // return res.status(200).json(payload);
  });
};
