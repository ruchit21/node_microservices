const { PurchaseRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError } = require("../utils/app-errors");

// All Business logic will be here
class PurchaseService {
  constructor() {
    this.repository = new PurchaseRepository();
  }

  async GetCartdetails(){
    try {
      const CartDetails = await this.repository.CartDetails();

      return FormateData(CartDetails);
    } catch (error) {
      throw new APIError(error.message)
    }
  }

  async ManageCart(userId, courseId, price, isRemove){
    try {
      const CartResults = await this.repository.AddToCart(userId, courseId, price, isRemove)

      return FormateData(CartResults);
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async PlaceOrder(UserInputs) {
    try {
      const id = UserInputs;

      const PurchaseResult = await this.repository.CreateOrder(id);
      return FormateData(PurchaseResult);
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async Events(payload) {

    const { event, data } = payload;

    const { userId, courseData } = data;

    const { id, price } = courseData;

    switch(event){
      case 'Add_TO_CART':
          this.ManageCart(userId, id, price, false);
          break;
      case 'REMOVE_FROM_CART':
            this.ManageCart(userId, id, price, true);
            break;
      default:
          break;
    }
  }
  
}

module.exports = PurchaseService;
