// const { CustomerModel, AddressModel } = require('../models');

const { APIError, BadRequestError, STATUS_CODES, AppError } = require('../../utils/app-errors');
const db = require("../models");
const PurchaseModel = db.purchase_model;
const CartModel = db.cart_model;

//Dealing with data base operations
class PurchaseRepository {
    async AddToCart(userId, courseId, price, isRemove){
        try {
            const cart = await CartModel.findAll({ where: { user_id: userId } });
            if (cart.length > 0) {
                let isExist = false;

                cart.map(async (CartItem) => {
                    if (CartItem.course_id.toString() === courseId.toString() && CartItem.user_id.toString() === userId.toString()) {
                        isExist = true;
                        if (isRemove) {
                            const cart_remove = await CartModel.findOne({ where: { id: CartItem.id } });
                            return cart_remove.destroy();
                        }
                    }
                });

                if (!isExist) {
                    const cart = new CartModel({
                        user_id: userId,
                        course_id: courseId,
                        total_price: price
                    })

                    return await cart.save();
                }
                
                return cart;
            } else {
                const cart = new CartModel({
                    user_id: userId,
                    course_id: courseId,
                    total_price: price
                })

                return await cart.save();
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }

    async CartDetails() {
        try {
            const cartData = await CartModel.findAll();

            if (cartData) {
                return { 'status': true, 'message': "Data from the cart has been successfully located.", 'data': cartData };
            } else {
                return { 'status': false, 'message': "No information about your cart was found." };
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }

    async CreateOrder(id) {
        try {
            const cart = await CartModel.findAll({ where: { user_id: id } });
            let OrederData = [];

            if (cart.length > 0) {
                cart.map(async (CartItems) => {
                    const order = new PurchaseModel({
                        user_id: id,
                        course_id: CartItems.course_id,
                        total_price: CartItems.total_price
                    });
                    OrederData.push(order);
                    await order.save();

                    const cart_data = await CartModel.findOne({ where: { id: CartItems.id } });
                    await cart_data.destroy();
                });
            }

            return OrederData;
        } catch (error) {
            throw new APIError(error.message);
        }
    }
}

module.exports = PurchaseRepository;