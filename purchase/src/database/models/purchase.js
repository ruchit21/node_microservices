module.exports = (sequelize, Sequelize) => {
  const Purchase = sequelize.define("purchases", {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    user_id: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    course_id: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    total_price: {
      allowNull: false,
      type: Sequelize.DECIMAL(10, 2)
    },
  },
  {
    timestamps: true,
    freezeTableName: true, // Model tableName will be the same as the model name
  });
  return Purchase;
};
