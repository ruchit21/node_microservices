const config = require("../../config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.db,
    config.user,
    config.password,
    {
        host: config.host,
        dialect: config.dialect,
        operatorsAliases: false,
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.purchase_model           = require("./purchase")(sequelize, Sequelize);
db.cart_model               = require("./cart")(sequelize, Sequelize);

module.exports = db;