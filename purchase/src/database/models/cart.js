module.exports = (sequelize, Sequelize) => {
  const Cart = sequelize.define("carts", {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    user_id: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    course_id: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    total_price: {
      allowNull: false,
      type: Sequelize.DECIMAL(10, 2)
    },
  },
  {
    timestamps: false,
    freezeTableName: true, // Model tableName will be the same as the model name
  });
  return Cart;
};
