## Description

    This project is for understand how actually node js microservices are working.
## Architecture and Technology Stack

    Node js (express js)

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Installation

1. Clone This Project using SSH or HTTPS Link
2. By default folder name will node_microservices (You can change it)

## RUN COMMAND

```bash
cd course
npm i

cd purchase
npm i

cd user
npm i

cd gateway
npm i
```

## DATABASE

4. Create Blank Database In phpmyadmin With Database Collation `utf8mb4_general_ci`.

5. Set up database connection in course,purchase,user folder in src/config/index.js.

## RUN COMMAND

```bash
cd course
npm start

cd purchase
npm start

cd user
npm start

cd gateway
npm start
```