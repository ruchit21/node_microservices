const UserService = require("../services/user-service");
const UserAuth = require("./middlewares/auth");

module.exports = (app) => {
  const service = new UserService();

  app.post("/signup", async (req, res, next) => {
    try {

      const { name, email, password, phone, gender, role } = req.body;
      const { data } = await service.signUp({ name, email, password, phone, gender, role });
      return res.json(data);

    } catch (error) {
        next(error);
    }
  });

  app.post("/signin", async (req, res, next) => {
    try {
      
      const { email, password } = req.body;
      const { data } = await service.signIn({ email, password });
      return res.json(data);

    } catch (error) {
      next(error);
    }
  });

  app.get("/profile", UserAuth,  async (req, res, next) => {
    try {

      const { id } = req.user;
      const { data } = await service.userProfile(id);
      return res.json(data);

    } catch (error) {
      next(error);
    }
  });
};