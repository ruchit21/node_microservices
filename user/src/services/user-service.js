const { UserRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError } = require("../utils/app-errors");

// All Business logic will be here
class UserService {
  constructor() {
    this.repository = new UserRepository();
  }

  async signUp(userInputs) {
    try {
      
      const { name, email, password, phone, gender, role } = userInputs;
      let salt = await GenerateSalt();

      let userPassword = await GeneratePassword(password, salt);

      const existingCustomer = await this.repository.CreateUser({
        name,
        email,
        password: userPassword,
        salt,
        phone,
        gender,
        role
      });

      if (existingCustomer.status) {
        const token = await GenerateSignature({
          email: email,
          id: existingCustomer.id,
          role: existingCustomer.role
        });

        return FormateData({ id: existingCustomer.data.id, token })
      }

      return FormateData(existingCustomer);
    } catch (error) {
      throw APIError(error.message)
    }
  }

  async signIn(userInputs) {
    try {
      var message = 'No data found';
      const { email, password } = userInputs;

      const existingUser = await this.repository.FindUserByEmail({ email });

      if (existingUser) {
        const validPassword = await ValidatePassword(
          password,
          existingUser.password,
          existingUser.salt
        );

        if (validPassword) {
          const token = await GenerateSignature({
            email: existingUser.email,
            id: existingUser.id,
            role: existingUser.role
          });
          return FormateData({ id: existingUser.id, token });
        } else {
          message = 'You have entered wrong password.';
        }
      } else {
        message = 'Email is not found.';
      }

      return FormateData({ 'status': false, 'message': message });
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async userProfile(id) {
    try {
      
      const user_details = await this.repository.FindUserById(id);

      if (user_details) {
        return FormateData(user_details);
      } else {
        return FormateData({ 'status': false, 'message': "User Not Found" });
      }

    } catch (error) {
      throw new APIError(error.message);
    }
  }

}

module.exports = UserService;
