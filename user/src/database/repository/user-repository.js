// const { CustomerModel, AddressModel } = require('../models');

const { APIError, BadRequestError, STATUS_CODES } = require('../../utils/app-errors');
const db = require("../models");
const UserModel = db.user_model;

//Dealing with data base operations
class UserRepository {

    async CreateUser({ name, email, password, phone, gender, role, salt }) {
        try {
            
            const data = {
                name,
                email,
                password,
                salt,
                phone,
                gender,
                role
            }

            const UserResult = await UserModel.create(data);
            if (UserResult) {
                return {'status': true, 'message': 'User Created Successfully', 'data': UserResult};
            } else {
                return {'status': false, 'message': 'Error While Creating User', 'data': UserResult};
            }

        } catch (error) {
            throw APIError(error.message)
        }
    }


    async FindUserByEmail({ email }) {
        try {
            const existingUser = await UserModel.findOne({ where: { email: email } });

            return existingUser;
        } catch (err) {
            throw APIError('API Error', STATUS_CODES.INTERNAL_ERROR, 'FindUser ' + err.message)
        }
    }

    async FindUserById(id){
        try {
            const user_details = await UserModel.findOne({ where: { id: id } });

            return user_details;
        } catch (err) {
            throw APIError('API Error', STATUS_CODES.INTERNAL_ERROR, 'FindUser ' + err.message)
            
        }
    }

}

module.exports = UserRepository;