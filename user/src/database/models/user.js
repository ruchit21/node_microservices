module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("users", {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    name: {
      allowNull: false,
      type: Sequelize.STRING
    },
    email: {
      allowNull: false,
      type: Sequelize.STRING
    },
    password: {
      allowNull: false,
      type: Sequelize.STRING
    },
    salt: {
      allowNull: false,
      type: Sequelize.STRING
    },
    phone: {
      allowNull: false,
      type: Sequelize.STRING
    },
    gender: {
      type: Sequelize.ENUM,
      values: ["male", "female", "other"],
      defaultValue: "male"
    },
    role: {
      type: Sequelize.ENUM,
      values: ["teacher", "student"],
      defaultValue: "student"
    },
  },
  {
    timestamps: true,
    freezeTableName: true, // Model tableName will be the same as the model name
  });
  return User;
};
